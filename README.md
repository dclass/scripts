# Script
## Dart
### Test Coverage
run this command on your terminal to generate test coverage in html format
```sh
sh <(curl -s https://gitlab.com/dclass/scripts/-/raw/main/test_coverage_html.sh) 
```
